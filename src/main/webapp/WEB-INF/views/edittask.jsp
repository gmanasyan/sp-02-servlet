<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Редактировать задачу</title>
    <link rel="stylesheet" href="assets/css/style.css">
<body>

<div class="header">
    <a href="projects">Projects</a>
    <a href="tasks">Tasks</a>
</div>

<div class="box">

<h2>Edit Task</h2>

    <form method="post" action="add-task">
        <dl>
            <dd><input hidden type="text" value="${task.id}" size=40 name="id" required></dd>
        </dl>
        <dl>
            <dt>Project:</dt>
            <dd>
                <select name="project.id">
                    <option value=""></option>
                    <c:forEach items="${projects}" var="project">
                    <jsp:useBean id="project" type="ru.volnenko.se.entity.Project"/>
                        <option value="${project.id}"
                        <c:if test="${task.project.id == project.id}">
                        selected
                        </c:if>
                        >${project.name}</option>
                    </c:forEach>
                </select>
            </dd>
        </dl>
        <dl>
            <dt>Task name:</dt>
            <dd><input type="text" value="${task.name}" size=40 name="name" required></dd>
        </dl>
        <dl>
            <dt>Description:</dt>
            <dd><input type="text" value="${task.description}" size=40 name="description" required></dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><input type="date" value="<fmt:formatDate value="${task.dateBegin}" pattern="yyyy-MM-dd"/>" name="dateBegin" required></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><input type="date" value="<fmt:formatDate value="${task.dateEnd}" pattern="yyyy-MM-dd"/>" name="dateEnd" required></dd>
        </dl>
        <button type="submit">Save</button>
        <button onclick="window.history.back()" type="button">Cancel</button>
    </form>

</div>
</body>
</html>