<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Main Page</title>
    <link rel="stylesheet" href="assets/css/style.css">
<body>

<div class="header">
    <a href="projects">Projects</a>
    <a href="tasks">Tasks</a>
</div>

<div class="box">

<h2>Projects</h2>

<table border="1" cellpadding="10" cellspacing="0">

    <thead>
        <tr>
            <th>#</th>
            <th>ID</th>
            <th>Name</th>
            <th>Date Begin</th>
            <th>Date End</th>
            <th>View</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>

    <c:set var="count" value="0" scope="page" />

    <c:forEach items="${projects}" var="project">
        <jsp:useBean id="project" type="ru.volnenko.se.entity.Project"/>
        <tr>
            <td>
                    <c:set var="count" value="${count + 1}" scope="page"/>
                    ${count}
            </td>
            <td>
                    ${project.id}
            </td>
            <td>
                    ${project.name}
            </td>
            <td>
                <fmt:formatDate value="${project.dateBegin}" pattern="yyyy-MM-dd"/>
            </td>
            <td>
                <fmt:formatDate value="${project.dateEnd}" pattern="yyyy-MM-dd"/>
            </td>
            <td>
                <a href="view-project?projectId=${project.id}">View</a>
            </td>
            <td>
                <a href="edit-project?projectId=${project.id}">Edit</a>
            </td>
            <td>
                <a href="remove-project?projectId=${project.id}">Remove</a>
            </td>
        </tr>
    </c:forEach>
</table>
<br/><br/>
<a class="button" href="add-project">Add Project</a>
</div>
</body>
</html>