package ru.volnenko.se.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.*;
import ru.volnenko.se.controller.Bootstrap;

/**
 * @author Denis Volnenko
 */
public abstract class AbstractCommand {

    @Autowired
    protected IProjectService projectService;

    @Autowired
    protected ITaskService taskService;

    @Autowired
    protected ITerminalService terminalService;

    @Autowired
    protected IDataService dataService;

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

}
