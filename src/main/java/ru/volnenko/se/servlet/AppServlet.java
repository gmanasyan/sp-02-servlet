package ru.volnenko.se.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/service")
public class AppServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String a = req.getParameter("id");
        resp.getWriter().println("<h1>Main Servlet</h1><br/><h2>" + a + "</h2>");
        resp.getWriter().println(req.getSession().getId());
        resp.getWriter().println(new Date(req.getSession().getCreationTime()));
        resp.getWriter().println("---");


    }
}
