package ru.volnenko.se.controller.mvcController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @GetMapping("tasks")
    public ModelAndView tasks() {
        List<Task> listTask = taskService.getListTask();
        ModelAndView model = new ModelAndView();
        model.setViewName("tasks");
        model.addObject("tasks", listTask);
        return model;
    }

    @GetMapping("add-task")
    public ModelAndView addTask() {
        List<Project> listProject = projectService.getListProject();
        ModelAndView model = new ModelAndView();
        model.setViewName("addtask");
        model.addObject("projects", listProject);
        return model;
    }

    @PostMapping("add-task")
    public String addTask(Task task) {
        taskService.merge(task);
        return "redirect:tasks";
    }

    @GetMapping("view-task")
    public ModelAndView viewTask(@RequestParam("taskId") String taskId){
        ModelAndView model = new ModelAndView();
        model.setViewName("viewtask");
        model.addObject("task", taskService.getTaskById(taskId));
        return model;
    }

    @GetMapping("edit-task")
    public ModelAndView editTask(@RequestParam("taskId") String taskId) {
        List<Project> listProject = projectService.getListProject();
        ModelAndView model = new ModelAndView();
        model.setViewName("edittask");
        model.addObject("projects", listProject);
        model.addObject("task", taskService.getTaskById(taskId));
        return model;
    }

    @GetMapping("remove-task")
    public String removeTask(@RequestParam("taskId") String taskId) {
        taskService.removeTaskById(taskId);
        return "redirect:tasks";
    }

}
