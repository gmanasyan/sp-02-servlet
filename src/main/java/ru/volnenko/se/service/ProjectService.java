package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.repository.ProjectRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
@Transactional(readOnly = true)
public class ProjectService implements IProjectService {

    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    @Transactional
    public Project createProject(final String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = new Project(name);
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public Project merge(final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Transactional
    public void merge(Collection<Project> projects) {
        projectRepository.saveAll(projects);
    }

    @Override
    public Project getProjectById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void removeProjectById(final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    @Override
    public List<Project> getListProject() {
        return projectRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void merge(final Project... projects) {
        if (projects == null || projects.length == 0) return;
        projectRepository.saveAll(Arrays.asList(projects));
    }

    @Override
    @Transactional
    public void removeByName(String name) {
        if (name == null) return;
        projectRepository.delete(projectRepository.findByName(name));
    }

}
