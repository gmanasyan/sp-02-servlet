package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.ProjectRepository;
import ru.volnenko.se.repository.TaskRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
@Transactional(readOnly = true)
public class TaskService implements ITaskService {

    private final TaskRepository taskRepository;

    private final ProjectRepository projectRepository;

    @Autowired
    public TaskService(
            final TaskRepository taskRepository,
            final ProjectRepository projectRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    @Transactional
    public Task createTask(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.save(new Task(name));
    }

    @Override
    public Task getTaskById(final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Task merge(final Task task) {
        return taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeTaskById(final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public Task createTaskByProject(final String projectName, final String taskName) {
        final Project project = projectRepository.findByName(projectName);
        if (project == null) return null;
        Task task = new Task(taskName);
        task.setProject(project);
        task = taskRepository.save(task);
        return task;
    }

    @Override
    public Task getByName(String taskName) {
        return taskRepository.findByName(taskName);
    }

    @Override
    @Transactional
    public void merge(Task... tasks) {
        merge(Arrays.asList(tasks));
    }

    @Override
    @Transactional
    public void merge(Collection<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void removeTaskByOrderIndex(String taskName) {
        taskRepository.delete(taskRepository.findByName(taskName));
    }

}
